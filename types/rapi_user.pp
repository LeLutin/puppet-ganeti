# @summary User that can be used to authenticate to ganeti's RAPI
#
# @see http://docs.ganeti.org/ganeti/current/html/rapi.html#users-and-passwords
# @see http://docs.ganeti.org/ganeti/current/html/rapi.html#rapi-access-permissions
#
# @note Options can be an empty array, which means that no special permissions
#   are given. The user is, however, still authenticated and can perform some
#   requests on the RAPI (see the "Access permissions" section of upstream
#   documentation linked above)
#
type Ganeti::Rapi_user = Struct[{
  username => String[1],
  password => String[1],
  options  => Array[Enum['write', 'read']],
}]
