# @summary Install extstorage scripts to manage zfs volumes
#
# With this extstorage installed, you need to enable the "ext" disk template on
# your cluster:
#
#   `gnt-cluster modify --enabled-disk-templates plain,drbd,ext`
#   `gnt-cluster modify --ipolicy-disk-templates plain,drbd,ext`
#
# You can then install instances on zvols by adding something like
# `--disk 0:size=60G,provider=zfs,zfs=POOLNAME` to a gnt-instance add command.
#
# The scripts for the extstorage come from (no license specified):
#   https://github.com/candlerb/ganeti-zfs
#
class ganeti::extstorage::zfs {
  file { '/usr/share/ganeti/extstorage/zfs':
    ensure  => directory,
    owner   => 'root',
    group   => 0,
    mode    => '0755',
    source  => 'puppet:///modules/ganeti/extstorage/zfs',
    recurse => true,
  }
  file { '/usr/share/ganeti/extstorage/zfs/defaults.sh':
    ensure => link,
    target => 'etc/defaults.sh',
    owner  => 'root',
    group  => 0,
    mode   => '0644',
  }
}
